---
title:
  - Open Source Search Engines
author:
  - Dionisis Nikolopoulos
theme:
  - Copenhagen
---
  
# Introduction
## Internet usage facts
- 1,331.9% increase since 2000.

- 5 billion people are internet users.

- Nearly 8 billion people is estimated to be the world's population. (March 2021)

An estimated 37% of people still have not used the internet.

### Straightforward conclusion

There is still an ever-growing market for the internet as a whole; and by
extension, for search engines.
It is safe to assume that search engines are being accessed by nearly every
person that can afford an internet connection.

# The need for privacy
## Mainstream methods
As of 2021, Google processes 3.6 billion searches in a day alone.
The proceeds from these searches are made from advertising:

+ Personalizing Advertisements
+ Promoting Specific Results through the Google Ads program.

Other search engines employing this:

+ Bing
+ Yahoo
+ Badoo
+ Yandex

---

![How Ad Personalisation works in search engines.](./img/ads.png "a title")

---

### Reasons this is disliked in the privacy conscious community
+ **More personalisation** results in more immersive user experience, which in turn
results in **more money** earned in general.
+ **More tracking** results in better behavioral and interest mapping, leading into
yet **more profits**.
+ **More sharing** of the aforementioned information (user profiles etc.)
to advertisers is **even more profitable**. 

#### Unfortunate Conclusion
Financial incentive is **antithetic** to adherence to user information.

However, seeing how the market relating to search engines is shaped, it is
reasonable to assume **privacy-protecting practices are not as profitable as
the sale of private user information**.
Especially when it comes to running an open source search engines.

# Current Technologies

## What is a crawler?
A **web crawler** is a program that automatically searches the internet and
indexes every website that meets certain requirements, for the purpose of
these websites to be able to be searched and accessed from a
search engine.

---

![How a web crawler works](./img/crawler.png "a title")

---

### Concern
When used by a general internet search engine, the program needs to scan the
entirety of the accessible internet.

#### Conclusion 
**Millions** of websites need be processed and indexed per day!
This is difficult to achieve when having a small or no team, without much
funding. Thus, **it is hard to make a competitive open source crawler.**

How can this be solved?

---

## Metasearch Engines

### Why are they needed?
Since making a crawler is an enormous undertaking for open source developers,
metasearch engines, or search aggregators, use crawlers from **other search
engines**, like Google and Bing.

### Notable examples:

+ Startpage (Google's Results)
+ Searx (Up to 70 search engines' results)

Searx is fully open source and will be the focus of the next chapter.

---

![How Searx works](./img/searx.png "a title")

---

### Why is this special?
+ It is **private**. Search result and Advertisement personalisation is
  circumvented.
+ It **rewards innovation**. An instance could be running a fork of the Searx
  project, adding specific features.
+ It is **secure**. The code every instance is running must be publicly
  available, making inspection of source code easy, as way to detect malicious
  behavior.

---

## Smaller scale search engines

### Good news!
This is an area open source dominates. Developers often use search engines
to make searching databases of websites easier, tweaking the code as needed.

### Notable examples:

+ Apache Lucene
+ Typesense
+ Elasticsearch

# What lies ahead

## Uncertainty!

### Most important problems

+ Smaller profits than mainstream text engines. (To be expected)
+ Heavy maintenance of projects needed.
+ People need to be technically proficient to operate these search engines and
keep privacy online.

#### The bright side

Studies have shown that the amount of digital natives increases, **making a 
direct correlation of age with computer literacy**.\

##### Conclusion

This might bring an increase of awareness about societal issues that rise from
widespread technology usage.
Internet privacy is one of the most prominent of these issues.

# Overall Conclusions

### Recent developments

+ Search engines are now integrated into our daily lives.
+ Every facet of our personality and interests can be deducted through the
information that we give away for free.
+ The internet has unified the voices of people with a brewing distrust of big tech.
+ Open source technologies and privacy-first applications very often coincide.

---

### Bottom Line

Open source allows for the literal maximum of access to the internal
functions of the program.
**Overall, open source search engines are spawning a new market**. Today,
they might be obscure compared to mainstream competitors, but in a few
years, with refinements and the with public interest continuing to develop
open source engines could be a part of a new era in internet privacy, one that
metasearch engines might lead.

---

Thank you for your time!

This presentation is open source!

https://gitlab.com/nnis/information-recovery-open-source

![](./img/search.png "")
